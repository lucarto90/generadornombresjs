//
document.querySelector('#generar-nombre').addEventListener('submit', cargarNombres);

// llamado a ajax e imprimir resultados
function cargarNombres (e) {
    e.preventDefault();
    
    // leer las variables
    const origen = document.getElementById('origen');
    const origenSeleccionado = origen.options[origen.selectedIndex].value;

    const genero = document.getElementById('genero');
    const generoSeleccionado = genero.options[genero.selectedIndex].value;

    const cantidad = document.getElementById('numero').value;

    let url = 'https://uinames.com/api/?';

    // si hay origen agregarlo a la url
    if (origenSeleccionado !== '') {
        url += `region=${origenSeleccionado}&`;
    }

    // si hay genero agregarlo a  la url
    if (generoSeleccionado !== '') {
        url += `gender=${generoSeleccionado}&`;
    }
    
    // si hay una cantidad. agregarlo a la url
    if (cantidad !== '') {
        url += `amount=${cantidad}`;
    }

    // conectar con ajax
    // iniciar XMLHttpRequest
    const xhr = new XMLHttpRequest();
    // abrimos la conexion
    xhr.open('GET', url, true);
    // obtenemos los datos e imprimimos
    xhr.onload = function () {
        if (xhr.status === 200) {
            const respuesta = JSON.parse(xhr.responseText);
            // generar el html
            let htmlNombres = '<h2>Nombres Generados</h2>';

            htmlNombres += '<ul class="lista">';

            // imprimir cada nombre
            respuesta.forEach(nombre => {
                htmlNombres += `
                    <li>${nombre.name}</li>
                `;
            });

            htmlNombres += '</ul>';

            document.getElementById('resultado').innerHTML = htmlNombres;
        }
    }
    // enviamos el request
    xhr.send();
}